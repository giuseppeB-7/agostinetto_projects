const borghiFade=function(){
	
		//const tabs=document.querySelectorAll('.contact-tabs-btn')

	
  var mySplitText = new SplitText(".purple .close p", {type:"chars, words"});
    var chars = mySplitText.chars; //an array of all the divs that wrap each character

		const tl=new TimelineMax({})

        .to('.purple .hiddenInfo',0.2,{display:'block',autoAlpha:1,ease: Power3.easeInOut},.03)
         .staggerFromTo('.hiddenInfo ul li', .3, {opacity: 0,y: 30}, {opacity: 1,y:0,ease: Power3.easeInOut}, 0.01)
        .to('.beige .square ',0.2,{display:'none', autoAlpha:0,ease: Power3.easeInOut},"-=1",)
        .to('.wrapperBorgo ',.6,{zIndex:1,autoAlpha:1,ease: Power3.easeInOut},.8,"-=0.9",)
        .to('.wrapperBorgo ',.6,{scale:1.04,ease: Power3.easeInOut},.3,"-=0.5",)
        .staggerFromTo(chars, .3, {opacity: 0,x:3}, {opacity: 1,x:0,ease: Power3.easeInOut}, 0.01)

        tl.pause()

	const triggerFaggio =document.querySelector('.purple .cta')

	const triggerCloseFaggio = document.querySelector('.purple .close')

	 
		triggerFaggio.addEventListener('click',function(){
			console.log('click')
			tl.play();
		})
 
    triggerCloseFaggio.addEventListener('click',function(){
			console.log('click')
			tl.reverse();
		})
    	// for (var i = tabButtons.length - 1; i >= 0; i--) {
	// 	tabButtons[i].addEventListener('click',function(){
			
	// 		tl.play();
	// 	})
	// }


//     $(".wineList__item img").on('mouseenter', function(e) {
//         console.log('hover')
//         $(this).parent('.wineList__item ').addClass('isActive')
//     var data_id = $(this).data('id');

//     // Shows the hovered floorplan, hides others
//     $('.wineBtm__text').each(function() {
//         var el = $(this);

//         if(el.attr('id') == data_id)
//             el.addClass('isActive')
//         else
//          el.removeClass('isActive')
//     });
// }).mouseleave(function() {
//       console.log('mouseleave')

//     $(this).parent('.wineList__item ').removeClass('isActive')
//      $('.wineBtm__text').removeClass('isActive')
//     });

  var mySplitTextLupo = new SplitText(".beige .close p", {type:"chars, words"});
    var charsLupo = mySplitTextLupo.chars; //an array of all the divs that wrap each character

		const tlUpo=new TimelineMax({})

        .to('.beige .hiddenInfo',0.2,{display:'block',autoAlpha:1,ease: Power3.easeInOut},.03)
         .staggerFromTo('.hiddenInfo ul li', .3, {opacity: 0,y: 30}, {opacity: 1,y:0,ease: Power3.easeInOut}, 0.01)
        .to('.purple .square ',0.2,{display:'none', autoAlpha:0,ease: Power3.easeInOut},"-=1",)
        .to('.wrapperBorgoLupo ',.2,{zIndex:1,autoAlpha:1,ease: Power3.easeInOut},.8,"-=0.9",)
        .to('.wrapperBorgoLupo ',.6,{scale:1.04,ease: Power3.easeInOut},.3,"-=0.5",)
        .staggerFromTo(charsLupo, .3, {opacity: 0,x:3}, {opacity: 1,x:0,ease: Power3.easeInOut}, 0.01)

        tlUpo.pause()

	const triggerLupo =document.querySelector('.beige .cta')

	const triggerCloseLupo = document.querySelector('.beige .close')

	 
		triggerLupo.addEventListener('click',function(){
			console.log('click')
			tlUpo.play();
		})
 
    triggerCloseLupo.addEventListener('click',function(){
			console.log('click')
			tlUpo.reverse();
		})


}






export default borghiFade
