import Barba from 'barba.js'
const projectTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */


    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */
     console.log('projectTransition')
     
      const projectBack = document.querySelector('.project-carousel-preview')
     const ProjectTitle = document.querySelector('.project-slide-title-container  .project-slide-title')
    

     const tl=new TimelineMax({onComplete:function(){
      this.fadeIn()
     }.bind(this)})
      .to('.project-slide-index',.3, {x:'300'})
      .to('.project-slide-client span span',.3, {y:'-100px'})
      .to('.project-slide-title-container  .project-slide-title',.3, {position:'absolute',ease: Power2.easeOut})
      .to('.project-slide-title-container',.5, {borderLeft:'0' })
      .to('.project-slide-title-container  .project-slide-title',.3, { className:"+=noPadding"},'+=0.01')
      .to('.project-carousel-element-img', .3, {x: -20,autoAlpha:0,ease: Power2.easeOut})
      .to('.project-carousel-container',.3, {marginBottom:'0'})
      .to('.project-carousel-preview',0.7, {scale:2,ease: Power2.easeOut})
      .to('.project-slide-title-container  .project-slide-title',.3, {fontSize:'3.8em',ease: Power2.easeOut})
      .to('.project-slide-title-container  .project-slide-title',.3, {letterSpacing : '38px',ease: Power2.easeOut},'+=0.01')
      .to('.project-carousel-preview',0.3, {scale:4.5,ease: Power4.easeOut},'+=0.1')
  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */


    console.log('next-fading-in')
    var _this = this;

    let tl=new TimelineLite({delay:0.5,onComplete:function(){
       _this.done();
    }})
    tl.to(this.oldContainer,.5, {opacity:0})
      .from(this.newContainer,.5,{opacity:0, visibility:'visible'},0)
  }
});

export default projectTransition