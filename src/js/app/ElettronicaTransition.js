import Barba from 'barba.js'
const ElettronicaTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

     /*var next=document.querySelector('.next-title-container').cloneNode(true)

     next.style.position='fixed'
     next.style.zIndex='999999'

     document.body.prepend(next)*/

    // As soon the loading is finished and the old page is faded out, let's fade the new page
  //   Promise
  //     .all([this.newContainerLoading, this.fadeOut()])
      
  // },


      Promise
        .all([this.newContainerLoading])
        .then(this.movePages.bind(this));
    },

      movePages: function() {
      var _this = this;
      var goingForward = true;
     
     this.updateLinks();

      if (this.getNewPageFile() === this.oldContainer.dataset.prev) {
        goingForward = false;
      }

      TweenLite.set(this.newContainer, {
        visibility: 'visible',
        yPercent: goingForward ? -100 : 100,
        position: 'fixed',
        left: 0,
        top: 0,
        right: 0
      });

      TweenLite.to(this.oldContainer, 0.4, { yPercent: goingForward ? -100 : 100 });
      TweenLite.to(this.newContainer, 0.4, { yPercent: 0, onComplete: function() {
        TweenLite.set(_this.newContainer, { clearProps: 'all' });
        _this.done();
      }});
    },
    
      updateLinks: function() {
          var PrevLink = document.querySelector('a.prev');
      PrevLink.href = this.oldContainer.dataset.prev;
      console.log('update links')
    },
       getNewPageFile: function() {
      return Barba.HistoryManager.currentStatus().url.split('/').pop();
    }


});

export default ElettronicaTransition