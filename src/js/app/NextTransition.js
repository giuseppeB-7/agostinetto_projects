import Barba from 'barba.js'
const NextTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

     /*var next=document.querySelector('.next-title-container').cloneNode(true)

     next.style.position='fixed'
     next.style.zIndex='999999'

     document.body.prepend(next)*/

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */
     console.log('next-fading-out')
     
     const title=document.querySelector('.next-title-container')
     title.style.position='fixed'
     title.style.bottom=0
     const tl=new TimelineMax({onComplete:function(){
      this.fadeIn()
     }.bind(this)})
     .to('.next-title-container', 0.5,{width:'100vw',className:"+=isFull"})
      .to('.next-section',0.5,{zIndex:999999})
     .to('.next-title-container',0.5,{height:'100vh',zIndex:999999, marginTop:0},'+=0.01')

    
  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */


    console.log('next-fading-in')
    var _this = this;

    let tl=new TimelineLite({delay:0.5,onComplete:function(){
       _this.done();
    }})
    tl.to(this.oldContainer,1.5, {opacity:0})
      .from(this.newContainer,1.5,{opacity:0, visibility:'visible'},0)
  }
});

export default NextTransition