

const dragSlider=function (){

var $slides = $(".slide");
var $container = $("#containerSlide");
var $slider = $("#slider");

var boxWidth = $container.width();
var sliderLength = boxWidth * $slides.length;
var targetX = 0;

function updateSlideCounter(){
    var counterText=(-1*targetX)+1
    if(counterText<10){
      counterText="0"+counterText
    }
    $('.activeNumber').text(counterText)
}

//console.clear();

$slides.outerWidth(boxWidth);
$slider.outerWidth(sliderLength);

// for (var i = 0; i <= $slides.length - 1; i++) {
//   var src =
//     "https://unsplash.it/" +
//     window.innerWidth * 0.7 +
//     "/" +
//     window.innerHeight * 0.5 +
//     "?random=" +
//     i;

//   TweenLite.set($slides[i], { backgroundImage: "url(" + src + ")" });
// }

// TweenMax.set('#progress', { transformOrigin: 'left', scaleX: 0});

$(window).resize(function() {
  boxWidth = $container.width();
  sliderLength = boxWidth * $slides.length;

  $slides.outerWidth(boxWidth);
  $slider.outerWidth(sliderLength);

  TweenMax.to($slider, 0.3, {
    x: boxWidth * targetX,
    onUpdate: function() {
      TweenMax.set("#progress", {
        scaleX:
          $slider[0]._gsTransform.x / (boxWidth * ($slides.length - 1)) * -1
      });
    }
  });
});

Draggable.create("#slider", {
  type: "x",
  edgeResistance: .3,
  bounds: "#containerSlide",
  throwProps: true,
  //onDrag: setProgress,
  
  onDragEnd:function(){
    
    targetX=this.endX * $slides.length / sliderLength

    setProgress()
    updateSlideCounter()
  },
   onDragStart:function(e) {
      console.log('dragg')
  },
  snap: {
    x: function(value) {
      return Math.round(value / boxWidth) * boxWidth;
    }
  }
  
});

var lastTarget = 0;

function setProgress() {
  console.log('update handle')
  // targetX = Math.round($slider[0]._gsTransform.x / boxWidth);
  // targetX =
  //   targetX < -1 * ($slides.length - 1) ? -1 * ($slides.length - 1) : targetX;
  // TweenMax.set("#progress", {
  //   scaleX: $slider[0]._gsTransform.x / (boxWidth * ($slides.length - 1)) * -1
  // });
  
  var percentScrub=-targetX /($slides.length-1)
  //var resultX=Math.floor(percentScrub * boxWidth)
  var resultX=Math.floor(percentScrub * boxWidth)-(percentScrub * $(handle).width());
  
  console.log(percentScrub * boxWidth, targetX, percentScrub, boxWidth, resultX)
  TweenMax.to(handle,0.5, { x: resultX, ease: Power4.easeOut });

  //lastTarget = targetX;
}

$("#prev").click(function() {
  if (targetX < 0) {
    

    targetX++;
    updateSlideCounter()
    setProgress()

    TweenMax.to($slider, 1, {
      x: boxWidth * targetX,
      //onUpdate: setProgress,
      ease: Power4.easeOut,
    });
  }
});

$("#next").click(function() {
  if (targetX > -1 * ($slides.length - 1)) {

    targetX--;
    updateSlideCounter()
    setProgress()

    TweenMax.to($slider, 1, {
      x: boxWidth * targetX,
      //onUpdate: setProgress,
      ease: Power2.easeOut,
    });
  }
});

// Updated code

var scrubber = document.getElementById("scrubber");
var handle = document.getElementById("handle");

//Select width of the scrubber
var scrubWidth = scrubber.getBoundingClientRect().width;
// Use scrubWidth to calculate handleWidth and use handleWidth wherever needed to keep your slides snap correctly, handleWidth depends on number of slides
var handleWidth = scrubWidth / ($slides.length - 1);

// ratio to calculate x translate
var ratio = scrubWidth / (sliderLength - boxWidth);

TweenMax.set("#handle", { xPercent: -50, yPercent: -50 });

var handleDraggable = new Draggable.create("#handle", {
  type: "x",
  //Calculate minX and maxX to set bounds
  bounds: { minX: 0, maxX: scrubWidth },
  throwProps: true,
  snap: {
    x: function(value) {
      return Math.round(value / handleWidth) * handleWidth;
    }
  },
  onPress:function() {
		console.log("press");
         TweenLite
             .to('.innerContainer',.3,{scale:'.7'},.2)
	},
  
   onDragStart:function(e) {
      console.log('dra two')
  

  },
   onDragEnd:function(e) {
      console.log('stop dra two')
            TweenLite
            //  .to('.innerContainer',.3,{scale:'1'},.2)
  },
     onRelease :function(e) {
      console.log('stop dra two')
            TweenLite
             .to('.innerContainer',.3,{scale:'1'},.2)
  },

  onDrag: updateSlides,
  onThrowUpdate: updateSlides
});

  function updateSlides() {
    // Set the position of slider by calculating ratio
    TweenMax.set($slider, { x: -this.x / ratio });
    var slidePercent=this.x/boxWidth
    targetX=-1*Math.floor(slidePercent*$slides.length)
    updateSlideCounter()
  }
}

export default dragSlider