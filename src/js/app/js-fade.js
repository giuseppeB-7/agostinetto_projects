const jsFade = function() {
    
$('.scrollaBle').each(function() {

var mySplitText = new SplitText(".pdp_heroText h1, h2", {type:"chars, words"});
var spliParagraph = new SplitText(".pdp_hero--top  p", {type:"lines"});

  var containerInner = $('.pdp_hero').outerHeight() * 3;
  var chars = mySplitText.chars; //an array of all the divs that wrap each character

  console.log('containerInner 2', containerInner)

  var controller = new ScrollMagic.Controller();
  var tl = new TimelineMax({
    play: true
  });

 tl.staggerFrom(chars, 0.3, {
      opacity: 0,
      y: 20,
      rotationX: -4,
      transformOrigin: "0% 50% -50",
      force3D: true,
      ease: Back.easeOut
    }, 0.03, "+=0")
    .staggerFrom('.pdp_hero--top  p  > div', 0.3, {
      opacity: 0,
      y: 20,
      rotationX: -4,
      transformOrigin: "0% 50% -50",
      force3D: true,
      ease: Back.easeOut
    }, 0.03, "+=0")
  .staggerFrom('.pdp_notest li', 0.3, {
      opacity: 0,
      y: 20,
      force3D: true,
      ease: Back.easeOut
    }, 0.06, "+=0")

  var scene = new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: "onCenter",
      // offset:'150',

    })

    .setTween(tl)
    .addTo(controller)

});



  $('.pdp_Note__pair').each(function() {


var paragraphNote = new SplitText(".platesNotesTrigger ", {type:"chars, words"});
 
    var controller = new ScrollMagic.Controller();


  var chars = paragraphNote.chars; //an array of all the divs that wrap each character
    var lines = $(this).find('.lines');
    var tl = new TimelineMax({
      play: true
    });


  
  tl 
  .staggerFrom('.wrapperNotes .lines', 0.2, {
        width: 0,
        x: 20,
 
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")

  .staggerFrom(chars, 0.8, {
        opacity: 0,
        x: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")
   
 

    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // reverse: false
      })
      .setTween(tl)
      .addTo(controller)



 

  });


  $('.pdp_Note__plates').each(function() {


var platesNote = new SplitText(".foodNotesTrigger", {type:"chars, words"});
 
    var controller = new ScrollMagic.Controller();


  var chars = platesNote.chars; //an array of all the divs that wrap each character
 
    var tl = new TimelineMax({
      play: true
    });


  
  tl
  .to('.platesNotesTrigger > div', 0.8, {
        opacity: 0,
        y: -20,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")

         .set('.foodNotesTrigger', {
       display:'block',
      })
          .set('.platesNotesTrigger', {
       display:'none',
      })
     .staggerFrom(chars, 0.8, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")
 

    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // reverse: false
      })
      .setTween(tl)
      .addTo(controller)



 

  });

  $('.js-section').each(function() {



    var controller = new ScrollMagic.Controller();


    var chars = $(this).find('p div');; //an array of all the divs that wrap each character
    var layerOne = $(this).find('.layer__one');
    var layerTwo = $(this).find('.layer__two');
      var img = $(this).find('img');
    var layer = $(this).find('.layer');
    var tl = new TimelineMax({
      play: true
    });


  
  tl.staggerFrom(chars, 0.8, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")
          .staggerFrom(img, 0.4, {
        opacity: 0,
           scale:.9,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")
 

 
    


    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // reverse: false
      })
      .setTween(tl)
      .addTo(controller)



    console.log('containerSlide', containerSlide)


  });



  $('.pdp_hero--center').each(function() {



    var controller = new ScrollMagic.Controller();


    console.log('containerSlide', containerSlide)

    var chars = $(this).find('p div'); //an array of all the divs that wrap each character
    var layerOne = $(this).find('.layer__one');
    var layerTwo = $(this).find('.layer__two');
    var wineSpecs = $('.wine_specs')
    var tl = new TimelineMax({
      play: true
    });


    tl.to(wineSpecs, 0.5, {
        x: '-30%',
        autoAlpha: 0,
        ease: Power3.easeIn,
        force3D: true,
      }, .5)
 
    
      .to('.fixedImg', 0.7, {
        scale: '.6',
        ease: Power3.easeIn,
        force3D: true
      }, .7)
         .to('.fixedCircle_js', 0.7, {
    opacity:1,
        }, 0.7, "+=.7")


    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onEnter",
        offset: '150',
      })
      .setTween(tl)
      .addTo(controller)
  });





  $('#triggerSvgMorellino').each(function() {


    var containerInner = $('.pdp_hero').outerHeight() * 3;

    console.log('containerInner 2', containerInner)



    var controller = new ScrollMagic.Controller();




    var tlSvg = new TimelineMax({
      play: true
    });



    tlSvg.to('.morelline_Svg', 0.5, {
      x: '-100%',
      opacity: 0,
      ease: Power3.easeIn,
      force3D: true,
    }, .2, "1")
      .to('.fixedCicler__left__js', .8, {
         rotation: 180,
         transformOrigin: "center right !important",
    
        }, 0.2, "1")
.to('.morelline_Svg', 0.5, {
     display:'none',
      force3D: true,
    }, .2, "1")

    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // offset:'150',
        duration: containerInner
      })

      .setTween(tlSvg)
      .addTo(controller)

  });

  $('.pdp_hero__last').each(function() {

 var  mySplitText = new SplitText(".formTitle h5", {type:"chars"})

    var containerInner = $('.pdp_hero').outerHeight() * 3;
    var chars = $(this).find('.formTitle h5 div');; //an array of all the divs that wrap each character

    console.log('containerInner 2', containerInner)

    var controller = new ScrollMagic.Controller();
    var tl = new TimelineMax({
      play: true
    });



    tl
    .staggerFrom(chars, 0.8, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")

            .to('.fixedCircle__js', .8, {
         scale: 0,
     
             }, 0.6, "+=.1")
.to('.fixedImg', 1, {
      opacity: 0,
      ease: Power3.easeIn,
      force3D: true,
         }, 0.06, "+=1")
    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // offset:'150',

      })

      .setTween(tl)
      .addTo(controller)

  });


 $('#pinnedHero').each(function() {

var  mySplitText = new SplitText(".pdp_heroText--pinned h4", {type:"chars"})
    var  splitTwo = new SplitText(".pdp_heroText--pinned p", {type:"lines"})

    var containerInner = $('.pdp_hero').outerHeight() * 3;
    var chars = $(this).find('.pdp_heroText--pinned h4 div');
     var lines = $(this).find('.pdp_heroText--pinned p div');
     var img = $(this).find('.pdp_pinned'); //an array of all the divs that wrap each character

    console.log('containerInner 2', containerInner)

    var controller = new ScrollMagic.Controller();
    var tl = new TimelineMax({
      play: true
    });


tl  

.fromTo('.notesPlate', .3, {opacity: 1,y: 0}, {opacity: 0,y:30,ease: Power3.easeInOut}, 0.01)
 .staggerFrom(chars, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")
      .staggerFrom(lines, 0.5, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "-=.4")
      .staggerFrom(img, 0.4, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "-=.3")
        .set('.water-fill__text ',{ className: '+=water-fill__text__trigger'})

    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // offset:'150',

      })

      .setTween(tl)
      .addTo(controller)

  });


$('.pdp_hero__form').each(function() {

 var mySplitText = new SplitText(".formTitle h5", {type:"chars, words"});

    var containerInner = $('.pdp_hero').outerHeight() * 3;
    var chars = mySplitText.chars; //an array of all the divs that wrap each character

    console.log('containerInner 2', containerInner)

    var controller = new ScrollMagic.Controller();
    var tl = new TimelineMax({
      play: true
    });

   tl.staggerFrom(chars, 0.2, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")
    .staggerFrom('.wrapperInput', 0.4, {
        opacity: 0,
        y: 20,
        force3D: true,
        ease: Back.easeOut
      }, 0.02, "+=0")

    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // offset:'150',

      })

      .setTween(tl)
      .addTo(controller)

  });




 TweenLite.defaultEase = Linear.easeNone;

var picker = document.querySelector(".pdp_hero__drag");
var cells = document.querySelectorAll(".wrapper-img");
var proxy = document.createElement("div");

var cellHeight = 750;
var rotationX = 8;

var numCells = cells.length;
var cellStep = 1 / numCells;
var wrapHeight = cellHeight * numCells;

var baseTl = new TimelineMax({ paused: true });

TweenLite.set(picker, {
  perspective: 1100,
  width: wrapHeight - cellHeight
});

for (var i = 0; i < cells.length; i++) {  
  initCell(cells[i], i);
}

var animation = new TimelineMax({ repeat: -1, paused: true })
  .add(baseTl.tweenFromTo(1, 2))

var draggable = new Draggable(proxy, {  
  // allowContextMenu: true,  
  trigger: picker,
  throwProps: true,
  onDrag: updateProgress,
  onThrowUpdate: updateProgress,
  snap: { 
    x: snapX 
  }
});

function snapX(x) {
  return Math.round(x / cellHeight) * cellHeight;
}

function updateProgress() {  
  animation.progress(this.x / wrapHeight);
}

function initCell(element, index) {
  
  TweenLite.set(element, {
    width: cellHeight,
    scale: .75,
    rotationY: rotationX,
    x: -cellHeight
  });
  
  var tl = new TimelineMax({ repeat: 1 })
    .to(element, 1, { x: "+=" + wrapHeight, rotationY: -rotationX }, 0)
    .to(element, cellStep, { color: "#009688", scale: 1, repeat: 1, rotateY:0, yoyo: true }, 0.5 - cellStep)
  
  baseTl.add(tl, i * -cellStep);
}



}

export default jsFade
