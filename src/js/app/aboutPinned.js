const aboutPinned = function() {


  $('.aboutPinned').each(function() {



    var controller = new ScrollMagic.Controller();
    var wrapper = $(this).find('.wrapper-text');
    var tl = new TimelineMax();



    tl



      .set(this, {
        className: '+=transOut'
      }, '+=.2')

      .to(wrapper, .3, {
        y: 30,
        opacity: 0,
        ease: Power4.easeOut
      }, .1)
      .to('.aboutPinned__layer', .3, {
        opacity: 0,
        ease: Power4.easeOut
      }, '-=.2')
      .to('.aboutPinnedBg', 1, {
        className: '+=zoom',
        ease: Power4.easeOut
      }, '-=.1')


    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 'onCenter',
        duration: '80%'
      })
      .setTween(tl)

      .addTo(controller)

  });

}

export default aboutPinned
