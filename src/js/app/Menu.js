const menuInit = function() {

    /// hide show menu on scroll

    let lastScrollTop = 0;
	const header = document.querySelector('.header-container');

  

    console.log('menu init')

 

    const tlMenu = new TimelineMax({
            paused: true
        })
        .set('.menuInside ',{x:0,display:'flex'})
          .set('.fixedImg',{ className: '+=dNone'})
             .set('.scrollaBle',{ zIndex:-1})
        .set('.burger-click-region ',{ className: '+=active',})
        .set('.burger-menu ',{ className: '+=is-open',})
    
        .to('.menuInside__nav .water',1.5,{y:0,display:'block',ease:Power4.easeIn},.04,"-=0.5")
        .set('.controlsWine, .controlsInner, #scrubber, .scrollDown',{opacity:0},"-=0.1")
        .staggerFromTo('.menuList li',.5,{autoAlpha:'0', y:10},{autoAlpha:'1', y:0},.04)
         .set('.contactFooter ',{ className: '+=borderTop',})
         .staggerFromTo('.menuList__address li',.5,{autoAlpha:'0', y:10},{autoAlpha:'1', y:0},.04)
        
   
     
        .reverse()

 
    

    const menuLink = document.querySelector('.menuTrigger');
    const menuClose = document.querySelector('.close-menu');

    const toggleMenu = function() {
        console.log('menuclick')

		//header.classList.remove("nav_down");
        // header.classList.remove("nav_up");
		
        tlMenu.reversed(!tlMenu.reversed());
        
         //anims[0].play()

    }

    menuLink.addEventListener("click", toggleMenu)
    //menuClose.addEventListener("click", toggleMenu)
   

 

    return {
        toggleMenu: toggleMenu
    }
}

export default menuInit