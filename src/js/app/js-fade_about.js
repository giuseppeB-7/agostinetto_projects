const jsFadeAbout = function() {
    
  $('.js-section__about').each(function() {



    var controller = new ScrollMagic.Controller();

  var mySplitTextH = new SplitText(".js-section__about .wrapper-text h4, .wrapper-text h5, .wrapper-text a", {type:"chars, words"});
   var mySplitTextP = new SplitText(".js-section__about .wrapper-text p", {type:"lines"});
    //var charsLupo = mySplitTextLupo.chars; //an array of all the divs that wrap each character

/// h4 chars
// lines
// h5 chars
// p
// img fade

    var chars = $(this).find('p div'); //an array of all the divs that wrap each character
    var heading = $(this).find('h4 div');
     var subHeading = $(this).find('h5 div');
      var a = $(this).find('a div');

    var lines = $(this).find('.lines');
    var layerTwo = $(this).find('.layer__two');
      var img = $(this).find('img');
    var a = $(this).find('a');
    var tl = new TimelineMax({
      play: true
    });


  tl.staggerFrom(heading, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")

 .staggerFrom(lines, 0.3, {
        height:0,
        force3D: true,
        ease: Power3.easeInOut
      }, 0.06, "+=0")
  .staggerFrom(subHeading, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=.1")
    .staggerFrom(chars, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")
      .staggerFrom(img, .3, {
        opacity: 0,
        force3D: true,
        ease: Power3.easeInOut
      }, 0.06, "+=.1")
    
     .staggerFrom(a, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")


    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // reverse: false
      })
      .setTween(tl)
      .addTo(controller)



 


  });

    $('.js-section__Two').each(function() {



    var controller = new ScrollMagic.Controller();

  var mySplitTextH = new SplitText(".js-section__Two .wrapper-text h4, .js-section__Two .wrapper-text h5", {type:"chars, words"});
   var mySplitTextP = new SplitText(".js-section__Two .wrapper-text p", {type:"lines"});
    //var charsLupo = mySplitTextLupo.chars; //an array of all the divs that wrap each character

/// h4 chars
// lines
// h5 chars
// p
// img fade

    var chars = $(this).find('p div'); //an array of all the divs that wrap each character
    var heading = $(this).find('h4 div');
     var subHeading = $(this).find('h5 div');
      var a = $(this).find('a div');

    var lines = $(this).find('.lines');
    var layerTwo = $(this).find('.layer__two');
      var img = $(this).find('img');
    var a = $(this).find('a');
    var tl = new TimelineMax({
      play: true
    });


  tl.staggerFrom(heading, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")

  .staggerFrom(subHeading, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.01, "+=.1")

 .staggerFrom(lines, 0.3, {
        width:0,
        force3D: true,
        ease: Power3.easeInOut
      }, 0.06, "+=0")
    .staggerFrom(chars, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")
      .staggerFrom(img, .3, {
        opacity: 0,
        force3D: true,
        ease: Power3.easeInOut
      }, 0.06, "-=1")
    
     .staggerFrom(a, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "+=0")


    var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: "onCenter",
        // reverse: false
      })
      .setTween(tl)
      .addTo(controller)



 


  });



}

export default jsFadeAbout
