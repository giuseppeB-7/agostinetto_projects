import TimelineMax from 'gsap/TimelineMax'
// import EasePack from 'gsap/EasePack'
import TweenMax from "gsap/TweenMax";
// import CSSRulePlugin from "gsap/CSSRulePlugin";
import Draggable from 'gsap/Draggable';
import Scrollbar from 'smooth-scrollbar';
import Barba from 'barba.js'
import menuInit from './Menu.js'
import WaterTransition from './transitions/WaterTransition.js'
import WineTransition from './transitions/WineTransition.js'
import BarbaFadeTransition from './transitions/BarbaFadeTransition.js'
import NextTransition from './NextTransition.js'
import WineUpTransition from './transitions/WineUpTransition.js'
 import initForm from './initForm.js'

//VIEWS

import Home from "./views/Home.js"
import About from "./views/About.js"
import Contact from "./views/Contact.js"
import Pdp from "./views/Pdp.js"
import Collection from "./views/Collection.js"
import notFound from "./views/notFound.js"

// import LeftTransition from './LeftTransition.js'
// import RightTransition from './RightTransition.js'
// import UpTransition from './UpTransition.js'
// import DownTransition from './DownTransition.js'
// import SoftwareTransition from './SoftwareTransition.js'
// import ContactTabs from './ContactTabs.js'
// import openInfo from './OpenInfo.js'
// import arrowListener from './arrowListener.js'
// import softwarePopup from './SoftwarePopup'
import heroInit from './heroInit.js'
import state from './state.js'
import jsFade from './js-fade.js'

import hoverMouseMove from './mouseMoveImg.js'
import aboutScroll from './aboutScroll.js'
import fadeAbout from './fade-about.js'


import borghiFade from './borghiFade.js'

import dragSlider from './homeDragSlider.js'
import containerAnimation from './containerAnimation.js'
import aboutPinned from './aboutPinned.js'
import jsFadeAbout from './js-fade_about.js'
import sliderWebGl from './sliderAboutWebGl.js'
import soundWave from './soundWave.js'

document.addEventListener("DOMContentLoaded", function() {
 
initForm();
	//VIEWS INITIALIZATION

	Home.init();
	About.init();
  	Contact.init();
	Pdp.init();
Collection.init();
notFound.init();
soundWave()
	Barba.Pjax.start();

	
	var lastClicked
	


	Barba.Dispatcher.on('linkClicked', function(el) {
		
		 clicked=true
		state.lastClicked=el
    
    

	});

	let clicked=false;
	
	var history
  Barba.Dispatcher.on('initStateChange', function(currentStatus) {
	
     if(!clicked) {

      history=Barba.HistoryManager.history
      console.log('backButton', history)

      }

  });

  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container) {
    console.log('newPageReady')
    if(!clicked){
      status=currentStatus
    }

    clicked=false

  });


	//SELEZIONE TRANSIZIONI
	Barba.Pjax.getTransition = function() {

	
    if(state.lastClicked.getAttribute('data-link-type')=="wine-link")
    {
      
      //return WineTransition;
      return BarbaFadeTransition
    }
      else if(state.lastClicked.getAttribute('data-link-type')=="wine-transition-link")
    {
      
      //return WineTransition;
      return WineUpTransition
    }
	  return WaterTransition;
	};


  // ServiceWorker is a progressive technology. Ignore unsupported browsers
  if('serviceWorker' in navigator) {
    console.log('CLIENT: service worker registration in progress.');
    navigator.serviceWorker.register('./js/service-worker.js').then(function() {
      console.log('CLIENT: service worker registration complete.');
    }, function() {
      console.log('CLIENT: service worker registration failure.');
    });
  } else {
    console.log('CLIENT: service worker is not supported.');
}




 if ($("html").hasClass("touch")) {
  $('#containerSlide').css('overflow', 'scroll')
} else{
const scrollbar = Scrollbar.initAll();
}

 

 
  var PrevLink = document.querySelector('a.prev');
  var NextLink = document.querySelector('a.next');


var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
	xH = 0,
    yH = 0,
    friction = 1 / 30,
	frictionH = 1 / 90;

function moveBackground() {
  x += (lFollowX - x) * friction;
  y += (lFollowY - y) * friction;

    xH += (lFollowX - x) * frictionH;
  yH += (lFollowY - y) * frictionH;
  
  var translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';
    var translateHeading = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';


  $('.slideImg').css({
    '-webit-transform': translate,
    '-moz-transform': translate,
    'transform': translate
  });




  window.requestAnimationFrame(moveBackground);
}

$(window).on('mousemove click', function(e) {

  var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
  var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
  lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
  lFollowY = (10 * lMouseY) / 100;

});

moveBackground();



// new hoverEffect({
// parent: document.querySelector('.slide'),
//   intensity1: 0.1,
//   intensity2: 0.1,
//   angle2: Math.PI / 2,
//   image1: 'https://source.unsplash.com/Le7_qK9JaLU/1600x900',
//   image2: 'https://source.unsplash.com/XAqaeyzj3NM/1600x900',
//   displacementImage: 'https://cdn.rawgit.com/robin-dela/hover-effect/b6c6fd26/images/stripe1mul.png?raw=true'
// });



	

	
})