const contactEnter = function() {



  var mySplitTextH = new SplitText(".wrapper-text-list h1,.wrapper-text-list h2 ", {
    type: "chars, words"
  });
  var mySplitTextP = new SplitText(".wrapper-text-list p", {
    type: "lines"
  });

 
  var chars = mySplitTextH.chars; //an array of all the divs that wrap each character
 var lines = mySplitTextP.lines;


  const tlContactEnter = new TimelineMax({

      onComplete: function() {

      }
    })

   tlContactEnter
      .set('.contactView', {
        opacity: 1,
     
      })
   .staggerFrom(chars, 0.2, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")
    .staggerFrom(lines, 0.8, {
        opacity: 0,
        y: 20,
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "-=1")
         .staggerFrom('.contact-row', 0.8, {
        opacity: 0,
        y: 20,
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "-=1")
      .staggerFrom('.contactFooter ul', 0.8, {
        opacity: 0,
        y: 20,
        force3D: true,
        ease: Back.easeOut
      }, 0.06, "-=1")


}

export default contactEnter
