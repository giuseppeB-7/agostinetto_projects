const cursorRounded = document.getElementById("cursor");
const cursorClick = document.getElementById("cursorClick");
const triggerPopUp = document.querySelector('.info-news--trigger');

const openInfo= function() {
    
    console.log('initializing')
    try{
     $('.info-news--trigger').on('click', function(e) {
        console.log('hei')
         
        if(this.classList.contains('open')){
            const tl = new TimelineMax()
          
            .to('.info-newx--popUp', .5, {
                    
                    opacity:0,
                    display:'none'
                })
            .to('.bang',0.5,{
                opacity:1,
                display:'inline'
            },0).to('.close-news',0.5,{
                opacity:0,
                display:'none'
            },0)
            .set(this,{className:'-=open'})
        }else{
            const tl = new TimelineMax()
          
            .fromTo('.info-newx--popUp', .5, {
                    
                    autoAlpha:.3
                }, {
                    display:'block',
                    autoAlpha:1,
                 
                    ease: Power2.easeOut
                })
            .to('.close-news',0.5,{
                opacity:1,
                display:'inline'
            },0).to('.bang',0.5,{
                opacity:0,
                display:'none'
            },0)
            .set('.info-news--trigger',{className:'+=open'})
        }
         
    
    }) 

    $('.close-news__mobile').on('click', function(e) {
        console.log('hei')
         
        
        const tl = new TimelineMax()
          
        .to('.info-newx--popUp', .5, {
                
                opacity:0,
                display:'none'
            })
        .to('.bang',0.5,{
            opacity:1,
            display:'inline'
        },0).to('.close-news',0.5,{
            opacity:0,
            display:'none'
        },0)
        .set('.info-news--trigger',{className:'-=open'})
        
         
    
    }) 
 }catch (err){
    console.log('triggerPopUp non trovato')
 }
}


export default openInfo