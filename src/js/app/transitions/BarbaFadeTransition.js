import Barba from 'barba.js'
import state from '../state.js'
import {
  TimelineMax
} from 'gsap';

const BarbaFadeTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
    console.log('wine transition')
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */

    const img = state.lastClicked.querySelector('img')


    // img.style.top="-5vh";
    // img.style.left="250px";
    $('.winelist .fixedCircle').addClass('hide');
    img.style.height = img.offsetHeight
    console.log(img.offsetHeight)
    img.style.position = "fixed";
    img.style.width = "auto";

    img.style.zIndex = "99999"
    state.lastClicked.parentNode.setAttribute('style', '')

    if ($(window).width() > 1024) {
              console.log('big 1025')
      var tl = new TimelineMax({
          onComplete: function() {
            this.fadeIn()
          }.bind(this)
        })
        .to(img, .5, {
          top: "-5vh",
          left: "250px",
          height: "110vh",
          ease: Power3.ease
        }, .4)


        .timeScale(.5)

    } else {
  
      console.log('less 1025')
      var tl = new TimelineMax({
          onComplete: function() {
            this.fadeIn()
          }.bind(this)
        })
        .to(img, .5, {
          top: "14vh",
          left: "153px",
          height: "74vh",
          ease: Power3.ease
        }, .4)


        .timeScale(.5)
    }

  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */

    var _this = this;
    var $el = $(this.newContainer);


    //$(this.oldContainer).css('position','fixed');

    $el.css({
      visibility: 'visible',
      opacity: 0
    });

    var tl = new TimelineMax({
        onComplete: function() {
          _this.done();
        }
      }).to(this.oldContainer, .5, {
        opacity: 0
      }, 0)
      .set(this.newContainer, {
        opacity: 1
      }, .1)
      .timeScale(.5)



    // $el.animate({ opacity: 1 }, 10000, function() {
    //   /**
    //    * Do not forget to call .done() as soon your transition is finished!
    //    * .done() will automatically remove from the DOM the old Container
    //    */

    //   _this.done();
    // });


    $('.burger-menu').removeClass('isOpen')
    $('.burger-click-region').removeClass('active')
  }
});

/**
 * Next step, you have to tell Barba to use the new Transition
 */

export default BarbaFadeTransition
