import Barba from 'barba.js'




const WineUpTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */

  
     console.log('Wine up transition')

     let tl=new TimelineLite({onComplete:function(){
      this.fadeIn()
      
     }.bind(this)})

     return tl
       
        // .to('.water-transition ',1,{y:0, bottom:'100vh'   }, '+=.5')
        .set('body',{y:0,className: '+=startTransition'})
        .to('.water-transition__wine',.5,{y:0,className: '+=transition',ease:Power4.easeIn},.04,"+=0.1")
        .to('.water-transition__wine .water',1.5,{y:'0',ease:Power4.easeIn},.04,"+=.5")
        .to('.water-transition__wine ',1.5,{className: '+=transitionDown',ease:Power4.easeIn},"+=1")
        // .to('.menuInside__vertical .water',2,{y:'140%',display:'block',ease:Power4.easeIn},.04,"+=0.6")
        .to(this.oldContainer,.5, {opacity:0, display:'none'})
        
    //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */
    var _this = this;

    let tl=new TimelineLite({delay:0.1,onComplete:function(){
    	 _this.done();
    }})

    tl.from(this.newContainer,0.5,{opacity:0, visibility:'visible'})
    //$el.animate({ opacity: 1 }, 400, function() {
      /**
       * Do not forget to call .done() as soon your transition is finished!
       * .done() will automatically remove from the DOM the old Container
       */

     // _this.done();
    //});
            

         $('.burger-menu').removeClass('isOpen')
         $('.burger-click-region').removeClass('active')
  }
});

export default WineUpTransition