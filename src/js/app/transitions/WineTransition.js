//import Barba from 'barba.js'
// import state from "../state.js"




// const WineTransition = Barba.BaseTransition.extend({
//   start: function() {
//     /**
//      * This function is automatically called as soon the Transition starts
//      * this.newContainerLoading is a Promise for the loading of the new container
//      * (Barba.js also comes with an handy Promise polyfill!)
//      */

//     // As soon the loading is finished and the old page is faded out, let's fade the new page
//     console.log('wine transition')
//     Promise
//       .all([this.newContainerLoading, this.fadeOut()])

   
      
//   },

//   fadeOut: function() {
//     /**
//      * this.oldContainer is the HTMLElement of the old Container
//      */

  
//      console.log('fading-------------------------------')

//      let tl=new TimelineLite({onComplete:function(){
//       this.fadeIn()
      
//      }.bind(this)})
     
//      return tl.to(state.lastClicked.querySelector('img'),.5,{
//         scale:1.8
//      })
     
//   },

//   fadeIn: function() {
//     /**
//      * this.newContainer is the HTMLElement of the new Container
//      * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
//      * Please note, newContainer is available just after newContainerLoading is resolved!
//      */
//     var _this = this;

//     let tl=new TimelineLite({delay:0.1,onComplete:function(){
//     	 _this.done();
//     }})

//     tl
//     .to(this.oldContainer,10, {opacity:0})
//     .from(this.newContainer,10,{opacity:0, visibility:'visible'},"-=0.5")
//     //$el.animate({ opacity: 1 }, 400, function() {
//       /**
//        * Do not forget to call .done() as soon your transition is finished!
//        * .done() will automatically remove from the DOM the old Container
//        */

//      // _this.done();
//     //});
//   }
// });

// export default WineTransition


import Barba from 'barba.js'
const WineTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    this.fadeOut()
  
      console.log('wine transition')
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */
     console.log('enter-fading-out')

     this.oldContainer.style.position='fixed'
     
     
  /*const layers=document.querySelectorAll('.layer')


  for (var i = 0; i < layers.length; ++i) {
   
    const tl=new TimelineLite()
    .to(layers[i], 0.5,{scale:layers[i].getAttribute('scale-rate')})
    
  }*/
  setTimeout(function(){
    this.fadeIn()
  }.bind(this),2000)
  
    
  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */

     console.log('enter-fading-in')
    var _this = this;
    
    Promise.all([this.newContainerLoading]).then(function(){

        let tl=new TimelineLite({onComplete:function(){
         _this.done();
      }})

      tl.set(_this.newContainer,{visibility:"visible",opacity:0})
      .to(_this.newContainer,10,{opacity:1},0)
        

         $('.burger-menu').removeClass('isOpen')
         $('.burger-click-region').removeClass('active')
    
    })


    
    
    //});
  }
});

export default WineTransition