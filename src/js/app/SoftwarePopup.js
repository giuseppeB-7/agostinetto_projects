const softwarePopup = function(){
	$('.software-projects li').click(function(){
		$('.software-popup').fadeIn()
		$('.software-popup').css('display','flex')
		const index=$(this).index()
		
		$($('.software-popup .popup-content').get(index)).fadeIn()
	})

	$('.close-popup').click(function(){
		$('.software-popup').fadeOut()
		$('.software-popup .popup-content').fadeOut()
	})
}

export default softwarePopup