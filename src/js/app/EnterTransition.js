import Barba from 'barba.js'
const EnterTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */
     console.log('enter-fading-out')

     this.oldContainer.style.position='fixed'
     
  const layers=document.querySelectorAll('.layer')


  for (var i = 0; i < layers.length; ++i) {
   
    const tl=new TimelineLite()
    .to(layers[i], 0.5,{scale:layers[i].getAttribute('scale-rate')})
    
  }
  
  setTimeout(function(){
     this.fadeIn()
  }.bind(this),500)
    
  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */

     console.log('enter-fading-in')
    var _this = this;

    let tl=new TimelineLite({onComplete:function(){
       _this.done();
    }})

    tl.to(this.oldContainer,0.5, {opacity:0})
      .from(this.newContainer,0.5,{opacity:0, visibility:'visible'},0)
    
    //});
  }
});

export default EnterTransition