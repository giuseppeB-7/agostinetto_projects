import Barba from 'barba.js'
import homeSplash from '../HomeSlider.js'
import dragSlider from '../homeDragSlider.js'
import menuInit from '../Menu.js'
import state from "../state.js"


import containerAnimation from '../containerAnimation.js'

const Home = Barba.BaseView.extend({
  namespace: 'home',
  onEnter: function() {

    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */
    function detectIE() {
      var ua = window.navigator.userAgent;

      // Test values; Uncomment to check result …

      // IE 10
      // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

      // IE 11
      // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

      // Edge 12 (Spartan)
      // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

      // Edge 13
      // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

      var msie = ua.indexOf('MSIE ');
      if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
      }

      // other browser
      return false;
    }



    if (detectIE()) // If Internet Explorer
    {

 
    }




    $('.nav.prev').hide();
    const tl = new TimelineMax({

        onComplete: function() {

        }
      })
      

    // The new Container is ready and attached to the DOM.

  },
  onEnterCompleted: function() {
     function createAudio(src) {
  output =  '<audio id="audio">';
  // you can add more source tag
  output +=  '<source loop autoplay src='+src+'" type="audio/mp3" />';
  output +=  '</audio>';
}

if ($(window).width() < 960) {
console.log('not responsivew')
}
else {
  console.log('sourcd')
 
    $("#sound").replaceWith('<audio id="sound" loop autoplay> <source src=img/piano.mp3 " type="audio/mp3" /></audio>');

}


      

 var audio = document.getElementById("sound"); 
 var stopSound = document.getElementById("sound_wave"); 
  var playSound = document.getElementById("playSound"); 

function playAudio() { 
    audio.play(); 
} 

function pauseAudio() { 
    audio.pause(); 
}


 playSound.addEventListener('click',function(){
   console.log('playAudio')
    playAudio()
    $('#playSound').removeClass('pointer')
    $('#sound_wave').removeClass('noPointer')
		})

	stopSound.addEventListener('click',function(){
     console.log('pauseAudio')
	  pauseAudio(); 
        $('#playSound').addClass('pointer')
    $(' #sound_wave').addClass('noPointer')

		})
 

    $('.lazy').each(function() {

      $(this).attr("src", $(this).attr("data-src"));
      $(this).removeAttr("data-src");
      console.log($(this)[0].outerHTML);
    });
    console.log('home-enter-completed')
    // openInfo();
 
try {
 // quando finisce la splash faccio vedere 
  document.querySelector('.loader-fill').classList.add('loader-fill--full');
    document.querySelector('.splashPage').classList.add('splashIn');
}
catch(error) {
  console.error(error);
  // expected output: SyntaxError: unterminated string literal
  // Note - error messages will vary depending on browser
}


    
    var percent_text = document.querySelector('.loader-percent');

    var duration = 3000;
    var timer = 0;
    var frame = 50;
    function update() {
        timer = timer + frame;
        var percent_loaded = Math.ceil(timer / duration * 100);
        if (percent_loaded > 100) {
            percent_loaded = 100;
        }

        percent_text.innerText = percent_loaded + "%";
        if (percent_loaded < 100) {
            setTimeout(update, 50);
        }
    }

    setTimeout(update, 50);

    $('.loader').css('display', 'flex');
        $('.loader').css('display', 'flex');
      var tlSplashLogo = new TimelineMax({repeat:-1, repeatDelay:1.5});

        tlSplashLogo.to('#logo_one ', 1.7, {morphSVG:"#bottle ", yoyo:true,repeat:1,ease: Power2.easeOut})

    setTimeout(function () {
        $('.loader').fadeOut();
         $('.splashPage').fadeOut();
        homeSplash();
    }, 4700);

     // homeSplash();


 
try {
 // The Transition has just finished.
       $('body').removeClass('startTransition')
}
catch(error) {
  console.error(error);
  // expected output: SyntaxError: unterminated string literal
  // Note - error messages will vary depending on browser
}
   

    dragSlider();
    containerAnimation();
    menuInit();


    $('.fixedCircle__noJs').css("opacity", '1');

  },
  onLeave: function() {

    // if(state.lastClicked.getAttribute('data-link-type')=="wine-link"){
    //     var tl= new TimelineMax().to(state.lastClicked.querySelector('img'),.5,{
    //               scale:1.8
    //            })
    //   }


    // A new Transition toward a new page has just started.
   },
  onLeaveCompleted: function() {
    console.log('home-leave-completed')

     // The Container has just been removed from the DOM.
  }
});

export default Home
