import Barba from 'barba.js'
import jsFade from '../js-fade.js'
import hoverMouseMove from '../mouseMoveImg.js'
import containerAnimation from '../containerAnimation.js'
import menuInit from '../Menu.js'
import fullpage from '../fullPageInit.js'
import {
  TimelineMax
} from 'gsap';

const Collection = Barba.BaseView.extend({
  namespace: 'collection',
  onEnter: function() {
  $('.controlsInner').css('display', 'none')
    $('[data-namespace="pdp"] #containerSlide .scrollaBle').css('opacity', '0')

    // The new Container is ready and attached to the DOM.
  },
  onEnterCompleted: function() {
 function createAudio(src) {
  output =  '<audio id="audio">';
  // you can add more source tag
  output +=  '<source loop autoplay src='+src+'" type="audio/mp3" />';
  output +=  '</audio>';
}

if ($('#playSound').hasClass('pointer')) {

} else{
  if ($(window).width() < 960) {
console.log('not responsivew')
}
else {
  console.log('sourcd')
 
    $("#sound").replaceWith('<audio id="sound" loop autoplay> <source src=img/piano.mp3 " type="audio/mp3" /></audio>');

}
}


      

 var audio = document.getElementById("sound"); 
 var stopSound = document.getElementById("sound_wave"); 
  var playSound = document.getElementById("playSound"); 

function playAudio() { 
    audio.play(); 
} 

function pauseAudio() { 
    audio.pause(); 
}


 playSound.addEventListener('click',function(){
   console.log('playAudio')
    playAudio()
    $('#playSound').removeClass('pointer')
    $('#sound_wave').removeClass('noPointer')
		})

	stopSound.addEventListener('click',function(){
     console.log('pauseAudio')
	  pauseAudio(); 
        $('#playSound').addClass('pointer')
    $(' #sound_wave').addClass('noPointer')

		})
 
    fullpage();
    var tl = new TimelineMax({
      delay: 1
    }).to('[data-namespace="pdp"] #containerSlide .scrollaBle', 0.5, {
      opacity: 1
    })


    // var mySplitText = new SplitText(".pdp_heroText p", {type:"lines"});

    console.log('web-enter-completed')
    // The Transition has just finished.

    console.log('ci siamo')
  $('body').removeClass('startTransition')
    hoverMouseMove();
    menuInit();
    setTimeout(function(){
          $('.wineList__itemFull').css('opacity', '1')
  }, 1000);


    $('.lazy').each(function() {

      $(this).attr("src", $(this).attr("data-src"));
      $(this).removeAttr("data-src");
      console.log($(this)[0].outerHTML);
      $('.fixedCircle__noJs').css("opacity", '1');


    });
  },
  onLeave: function() {
    console.log('collection-leave')
             $('.wineList_container').css('opacity', '0');

        setTimeout(function(){
                 fullpage_api.destroy('all');

  }, 200);
       setTimeout(function(){
        console.log('destroy full page')
   
               
  }, 1400);

    $(document).off('keyup')
    // A new Transition toward a new page has just started.
  },
  onLeaveCompleted: function() {
    console.log('elettronica-leave-completed')
  
    // The Container has just been removed from the DOM.
           $('.wineList_container').css('opacity', '1');
  }
});

export default Collection;
