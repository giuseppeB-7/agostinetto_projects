import Barba from 'barba.js'
import containerAnimation from '../containerAnimation.js'
import aboutPinned from '../aboutPinned.js'
import jsFadeAbout from '../js-fade_about.js'
import sliderWebGl from '../sliderAboutWebGl.js'
import borghiFade from '../borghiFade.js'
import menuInit from '../Menu.js'

const About = Barba.BaseView.extend({
    namespace: 'about2',
    onEnter: function() {
            
              console.log('about-enter')
  $('.controlsInner').css('display', 'none')
         // The new Container is ready and attached to the DOM.
    },
    onEnterCompleted: function() {
         function createAudio(src) {
  output =  '<audio id="audio">';
  // you can add more source tag
  output +=  '<source loop autoplay src='+src+'" type="audio/mp3" />';
  output +=  '</audio>';
}

if ($('#playSound').hasClass('pointer')) {

} else{
  if ($(window).width() < 960) {
console.log('not responsivew')
}
else {
  console.log('sourcd')
 
    $("#sound").replaceWith('<audio id="sound" loop autoplay> <source src=img/piano.mp3 " type="audio/mp3" /></audio>');

}
}



  $('body').removeClass('startTransition')
      

 var audio = document.getElementById("sound"); 
 var stopSound = document.getElementById("sound_wave"); 
  var playSound = document.getElementById("playSound"); 

function playAudio() { 
    audio.play(); 
} 

function pauseAudio() { 
    audio.pause(); 
}


 playSound.addEventListener('click',function(){
   console.log('playAudio')
    playAudio()
    $('#playSound').removeClass('pointer')
    $('#sound_wave').removeClass('noPointer')
		})

	stopSound.addEventListener('click',function(){
     console.log('pauseAudio')
	  pauseAudio(); 
        $('#playSound').addClass('pointer')
    $(' #sound_wave').addClass('noPointer')

		})
 
 
  //   aboutScroll();
  //   fadeAbout();
  //   plains();
      menuInit();
      containerAnimation();
      aboutPinned();
      borghiFade();
      jsFadeAbout();
      sliderWebGl();
        $('.lazy').each(function() {
    
        $(this).attr("src",$(this).attr("data-src"));
        $(this).removeAttr("data-src");
        console.log($(this)[0].outerHTML);
           $('.fixedCircle__noJs').css("opacity", '1');
    });

    },
    onLeave: function() {
        console.log('web-leave')
        // A new Transition toward a new page has just started.
        //$(document).off('keyup')
    },
    onLeaveCompleted: function() {
        console.log('web-leave-completed')
        // The Container has just been removed from the DOM.
    }
  });

  export default About;