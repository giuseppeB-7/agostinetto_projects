const fullPage = function() {

		// Initializing fullpage.js
		initialize(false);
 
		function initialize(hasScrollBar){
			$('#myContainer').fullpage({
				anchors: ['morellino', 'vermentino', 'olio', '4thpage'],
				menu: '#menuCollection',
				slidesNavigation: true,
			 
			 
                licenseKey:'OPEN-SOURCE-GPLV3-LICENSE',
				scrollingSpeed: 800,
				autoScrolling: true,
				scrollBar: hasScrollBar,
				fitToSection:true,
					onLeave: function( section, origin, destination, direction){
					var leavingSlide = this;

					 console.log(this,'slideLEave')
					 	 console.log(origin.index,'slideLEave')
						  	if(origin.index == 2){
							console.log(origin.index,'index')
							} else if (origin.index == 1){
								 console.log(origin.index,'index')
							}

				}
			});
		}
 
		$('#menuCollection li').click(function(){
		 
			console.log($(this))
				$('#menuCollection li').removeClass('isActive');
			$(this).addClass('isActive');
			// setTimeout(function(){ 
			// 				$(this).addClass('isActive');

			// }, 200);
			console.log('click')
 		});

		$('#turnOff').click(function(){
			//$(this).addClass('active').siblings().removeClass('active');
			$.fn.fullpage.parallax.destroy();
		});

		// Applying the different options when changing the select boxes
		$("select").change(function (){
			var fullPageOption = $(this).data('option');
			var value = $(this).val();

			if(fullPageOption === 'offset'){
				$.fn.fullpage.parallax.setOption('offset', value);
			}

			else if(fullPageOption === 'type'){
				$.fn.fullpage.parallax.setOption('type', value);
			}

			else if(fullPageOption === 'autoScrolling'){
				$.fn.fullpage.setAutoScrolling(value === 'true');
			}

			// scrollBar
			// Because there's no method `setScrollBar` we have to destryo and initialice fullPage.js again
			// but this time we'll initialice it again with the scrollBar:true option
			else{
				$.fn.fullpage.destroy('all');
				initialize(value === 'true');
			}
		});
}

export default fullPage