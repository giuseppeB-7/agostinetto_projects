const homeSplash=function (){
 
 	
  var mySplitText = new SplitText(" .first .slideText p, .first .slideText h4, .first .slideText a", {type:"chars, words"});
    var chars = mySplitText.chars; //an array of all the divs that wrap each character

    const tlHomeSplahs=new TimelineMax({

				onComplete:function(){
				
				}
			})
            	  .to('.fixedImg_home', .5, {
                    
           
                       className:"+=scales",
                    }, 0.2)
       
			   	  .to('.fixedImg_homeWrapper', .5, {
                       autoAlpha:0,
                       display:'none',
                        ease: Back.easeOut
                    },  "+=.2")
.staggerFrom(chars, 0.3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=0")
.staggerFrom('.controlsWine, .controls,#scrubber,.menuTrigger, .logo', .3, {
        opacity: 0,
        y: 20,
        rotationX: -4,
        transformOrigin: "0% 50% -50",
        force3D: true,
        ease: Back.easeOut
      }, 0.03, "+=.1")
       
      .staggerFrom('.soundWave__ct', .3, {
        opacity: 0,
         
        ease: Back.easeOut
      }, 0.03, "+=0");
       
      
 

  
}

export default homeSplash