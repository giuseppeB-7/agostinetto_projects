import EnterTransition from './EnterTransition.js'

const barbaInit = function(){
	var Homepage = Barba.BaseView.extend({
	  namespace: 'homepage',
	  onEnter: function() {
	  	console.log('home-enter')
	      // The new Container is ready and attached to the DOM.
	  },
	  onEnterCompleted: function() {
	  	console.log('home-enter-completed')
	      // The Transition has just finished.
	  },
	  onLeave: function() {
	  	console.log('home-leave')
	      // A new Transition toward a new page has just started.
	  },
	  onLeaveCompleted: function() {
	  	console.log('home-leave-completed')
	      // The Container has just been removed from the DOM.
	  }
	});

	// Don't forget to init the view!
	Homepage.init();

	var Projects = Barba.BaseView.extend({
	  namespace: 'projects',
	  onEnter: function() {
	  	console.log('projects-enter')
	      // The new Container is ready and attached to the DOM.
	  },
	  onEnterCompleted: function() {
	  	console.log('projects-enter-completed')
	      
	  },
	  onLeave: function() {
	  	console.log('projects-leave')
	      // A new Transition toward a new page has just started.
	  },
	  onLeaveCompleted: function() {
	  	console.log('projects-leave-completed')
	      // The Container has just been removed from the DOM.
	  }
	});

	var SingleProject = Barba.BaseView.extend({
	  namespace: 'single-project',
	  onEnter: function() {
	  	console.log('projects-enter')
	      // The new Container is ready and attached to the DOM.
	  },
	  onEnterCompleted: function() {
	  	console.log('projects-enter-completed')
	      
	  },
	  onLeave: function() {
	  	console.log('projects-leave')
	      // A new Transition toward a new page has just started.
	  },
	  onLeaveCompleted: function() {
	  	console.log('projects-leave-completed')
	      // The Container has just been removed from the DOM.
	  }
	});

	// Don't forget to init the view!
	SingleProject.init();

	Barba.Pjax.start();

	
	
}

export default barbaInit