
function initForm(){
     // Blast the text apart by letter.
    

    var tl=new TimelineMax()

        .fromTo('.thanks', .5, {
            autoAlpha: 0,
            display:'none',
            ease: Circ.easeOut
        }, {
                    display:'block',
            autoAlpha:1,
        }, .01)
        .staggerFromTo('.thanks h5', 1, {
            y: 20,
            opacity: 0
        }, {
            y: 0,
            opacity: 1,
            ease: Circ.easeOut
        }, 0.015)

    tl.pause();
    $(document).ready(function(){

        $("#frmemail").submit(function(event){
            event.preventDefault();
            console.log('form')
   
 
            $.ajax({
                type: "POST",
                url:      "./sendMail.php" ,
                dataType: "text",
                data: $("#frmemail").serialize(),
                success: function(data){
                    tl.play();
                    console.log('success')
                    alert('data', data)
                     
                        console.log()


                },
                error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message, 'ajax call error');
                }
            });
        });

    
    });
}

export default initForm






