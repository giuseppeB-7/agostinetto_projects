var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var mustache = require('gulp-mustache');
var beautify = require('gulp-html-beautify');
var server = require('gulp-server-livereload');
var jshint = require('gulp-jshint');
var minify = require('gulp-minify');
var image = require('gulp-image');
const autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
const rollup = require('rollup');
const resolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
const json =require('rollup-plugin-json');
var cssnano = require('gulp-cssnano');
var htmlmin = require('gulp-htmlmin');
var sourcemaps = require('gulp-sourcemaps');
 const stripDebug = require('gulp-strip-debug');
const imagemin = require('gulp-imagemin');


/********************************************************************
 *
 *
 *
 *           WATCH / DEV
 *
 *
 *
 **/

// Gulp Sass Task

gulp.task('sass', function() {
    return    gulp.src('./src/scss/{,*/}*.{scss,sass}')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({ style: 'expanded' }))
        .pipe(autoprefixer('last 2 version'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'))
        .pipe(gulp.dest('./dev/css'))
 });



// Compile mustache files and beautify them
gulp.task('mustache', function () {
    gulp.src('./src/*.html')
        .pipe(plumber())
        .pipe(mustache('data.json'))
        .pipe(beautify({indent_size: 2, end_with_newline: true}))
        .pipe(gulp.dest('./dev'));
});

// manage img optimize for production

gulp.task('image', function () {
    gulp.src('./src/img/*')
        .pipe(image({
            pngquant: true,
            optipng: true,
            zopflipng: true,
            jpegRecompress: false,
            jpegoptim: true,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            svgo: true,
            concurrent: 10
        }))
        .pipe(gulp.dest('./dev/img'));
});

// gulp.task('image', function () {
//     gulp.src('./src/img/*')
//       .pipe(imagemin([
//     imagemin.gifsicle({interlaced: true}),
//     imagemin.jpegtran({progressive: true}),
//     imagemin.optipng({optimizationLevel: 5}),
//     imagemin.svgo({
//         plugins: [
//             {removeViewBox: true},
//             {cleanupIDs: false}
//         ]
//     })
// ]))
//         .pipe(gulp.dest('./dev/img'));
// });


gulp.task('default', ['image']);


// FONT
gulp.task('fonts', function () {
    gulp.src('./src/fonts/*')
        .pipe(gulp.dest('./dev/fonts'));
});



// Manage Javascript
/*gulp.task('js', function () {
    gulp.src('./src/js/*.js')
    .pipe(concat('all.js'))
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        
        .pipe(gulp.dest("./dev/js"));
});*/

// Livereload
gulp.task('serve', function () {
     gulp.src('./dev')
        .pipe(server({
            livereload: false,
            directoryListing: false,
            open: true
        }));
 });

gulp.task('js', () => {
  return rollup.rollup({
    input: './src/js/app/index.js',
    plugins:[resolve(),
    json(),
                commonjs()]
    
  }).then(bundle => {
    return bundle.write({
      file: './dev/js/app.js',
      format: 'umd',
      name: 'app',
      sourcemap: true
    });
  });
});

// // FONT
// gulp.task('vendor', function () {
//     gulp.src('./src/js/vendors/*')
//         .pipe(gulp.dest('./dev/js/vendors'));
// });

gulp.task('vendor', function () {
  gulp.src('./src/js/vendors/*')
    .pipe(concat('vendor.js'))
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        
           .pipe(gulp.dest('./dev/js/vendors'));
});


// Create Gulp Default Task and serve on DEV

gulp.task('default', ['sass', 'mustache','vendor', 'js'], function () {
    gulp.watch('./src/scss/{,*/}*.{scss,sass}', ['sass']);
    gulp.watch('./src/partials/**/*.html', ['mustache']);
    gulp.watch('./src/*.html', ['mustache']);
    gulp.watch('./src/js/app/**/*.js', ['js']);
    gulp.watch('./src/img/*', ['image']);
});



/**********************************************************************
 *
 *
 *
 *           BUILD
 *
 *
 *
 **/
// Gulp Sass Task
gulp.task('sass-build', function () {
    gulp.src('./src/scss/{,*/}*.{scss,sass}')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({ style: 'expanded' }))
        .pipe(autoprefixer('last 2 version'))
        .pipe(cssnano())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'))
        .pipe(gulp.dest('./build/css'))
});




gulp.task('html-build', function() {
  return  gulp.src('./dev/*.html')
   // .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./build'));
});


// manage img optimize
gulp.task('image-build', function () {
    gulp.src('./src/img/*')
    
        .pipe(gulp.dest('./build/img'));
});




// Manage Javascript
gulp.task('js-build', function () {
    gulp.src( './dev/js/app.js')
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(stripDebug())
        .pipe(gulp.dest("./build/js"));
});


gulp.task('vendor-build', function () {
  gulp.src('./dev/js/vendors/*')
    .pipe(concat('vendor.js'))
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        
           .pipe(gulp.dest('./build/js/vendors'));
});

// FONT
gulp.task('fonts-build', function () {
    gulp.src('./src/fonts/*')
        .pipe(gulp.dest('./build/fonts'));
});

// Build tools
gulp.task('build', [ 'js-build', 'html-build', 'fonts-build','vendor-build', 'image-build', 'sass-build']);